package it.infomaxgroup.testprettyfacesprimefaces5uploadproblem.newpackage.controllers;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import org.primefaces.event.FileUploadEvent;

@Named
@RequestScoped
public class UploadController implements Serializable {

  static final Logger LOGGER = Logger.getLogger(UploadController.class.getName());
  
  public void handleFileUpload(FileUploadEvent event) {
    LOGGER.log(Level.INFO, "File {0} has been successfully uploaded...", event.getFile().getFileName());
  }
}
