package it.infomaxgroup.testprettyfacesprimefaces5uploadproblem.newpackage.controllers;

import java.io.Serializable;
import java.util.logging.Logger;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

@Named
@RequestScoped
public class TestController implements Serializable {
  static final Logger LOGGER = Logger.getLogger(TestController.class.getName());

  public TestController() {
  }

  public void index() {
  }

}
