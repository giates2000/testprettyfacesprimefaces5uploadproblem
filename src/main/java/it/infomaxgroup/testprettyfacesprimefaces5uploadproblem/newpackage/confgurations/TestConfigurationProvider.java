package it.infomaxgroup.testprettyfacesprimefaces5uploadproblem.newpackage.confgurations;

import javax.faces.event.PhaseId;
import javax.servlet.ServletContext;
import org.ocpsoft.rewrite.annotation.RewriteConfiguration;
import org.ocpsoft.rewrite.config.Configuration;
import org.ocpsoft.rewrite.config.ConfigurationBuilder;
import org.ocpsoft.rewrite.config.Invoke;
import org.ocpsoft.rewrite.el.El;
import org.ocpsoft.rewrite.faces.config.PhaseOperation;
import org.ocpsoft.rewrite.servlet.config.HttpConfigurationProvider;
import org.ocpsoft.rewrite.servlet.config.rule.Join;


@RewriteConfiguration
public class TestConfigurationProvider extends HttpConfigurationProvider {

  @Override
  public Configuration getConfiguration(final ServletContext context) {
    return ConfigurationBuilder.begin()
                
      // Index 
      .addRule(Join.path("/test").to("/test.xhtml"))
      .perform(PhaseOperation.enqueue(Invoke.binding(El.retrievalMethod("testController.index"))).after(PhaseId.RESTORE_VIEW));
            
  }

  @Override
  public int priority() {
    return 10;
  }
}
